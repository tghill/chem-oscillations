%% Solver for Brusselator ODE system
% Integrates Brusselator equations and makes timeseries and phase
% space plots.
clear all;close all
%% Parameters
a=0;
b=0;

tspan=[0,50];

%% Solve the equations: nonstiff, use basic ODE solver
x0=[2;2.5];
opts=odeset('RelTol',1e-8, 'AbsTol',1e-14);
[t,y]=ode45(@(t,y) f_brusselator(t,y,a,b),tspan,x0,opts);

% Plot the results
figure(1)
subplot(2,1,1)
title(sprintf('Brusselator Trajectory: a=%.3f, b=%.3f', a, b))
hold on
plot(t,y(1:end,1))
grid on
xlabel('Time')
ylabel('X')

subplot(2,1,2)
hold on
plot(t,y(1:end,2))
xlabel('Time')
ylabel('Y')
grid on

figure(2)
hold on
plot(y(1:end,1), y(1:end,2))
xlabel('X')
ylabel('Y')

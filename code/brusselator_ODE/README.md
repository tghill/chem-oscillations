# Brusselator ODE solver
The script `run_brusselator.m` runs the solver, and the `f_brusselator.m` function defines the RHS of the system.

This code is great for parameter exploration and informing the use of the reaction diffusion system. For example, the initial conditions used by default in the reaction diffusion code come from outputs of the ODE system. Two points out of phase on the limit cycle were chosen from this solver too initialize the reaction diffusion solver.

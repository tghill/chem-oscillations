function F=f_brusselator(t,y,a,b)
f1=a+y(1).^2.*y(2)-b*y(1)-y(1);
f2=b*y(1)-y(1).^2.*y(2);
F=[f1;f2];
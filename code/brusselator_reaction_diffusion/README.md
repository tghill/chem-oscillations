# Brusselator reaction diffusion code
This code solves the Brusselator reaction diffusion system using spectral differentiation for the diffusion and RK4 time stepping. This code is quite fast.

%% Solver for brusselator reaction diffusion model.

% Double periodic 2D domain, RK4 time stepping, Crank-Nicolson scheme
% for diffusion.

%% Grid parameters
N=512
z=linspace(-1,1,N+1);
z=z(1:end-1);
L=2.5;
[x,y]=meshgrid(L*z,L*z);

t=0;
%% Model parameters arameters
ru=5e-4; rv=1e-3;	% Diffusion for each component
a=1;			% model parameters
b=2.1;			% b>1+a^2 for oscillations

%% Model functions
f1=@(u,v) a + u.^2.*v - (b+1)*u;
f2=@(u,v) b*u - u.^2.*v

% Set initial conditions
r2=x.*x+y.*y;
u0=zeros(size(x)); v0=zeros(size(x));
expfactor1=exp(-(((x+0.5*L).^2 + y.^2).^8)/((0.25*L)^8));
expfactor2=exp(-(((x-0.5*L).^2 + y.^2).^8)/((0.25*L)^8));
expfactor3=exp(-(((x).^2 + (y+0.5*L).^2).^8)/((0.25*L)^8));
expfactor4=exp(-(((x).^2 + (y-0.5*L).^2).^8)/((0.25*L)^8));

expfactor = expfactor1+expfactor2 + expfactor3 + expfactor4;
u0=0.707 + (1.4775-0.7)*expfactor;
v0=2.3696 + (1.6221-2.3696)*expfactor;

u0=u0+0.1*randn(size(u0));
v0=v0+0.1*randn(size(v0));
u=u0; v=v0;

% Compute wavevectors
dk=pi/L;
ksvec(1)=0; 
for ii=2:(N/2)
   ksvec(ii)=ii-1;
   ksvec(N/2+ii)=-N/2 + ii -1;
end
ksvec=ksvec*dk;
[k2,l2]=meshgrid(ksvec.^2,ksvec.^2);
ksvec(N/2+1)=0;
[k,l]=meshgrid(ksvec,ksvec);
k2=k.*k; l2=l.*l;

%% Solver:
lapu=-ru*(k2+l2);
lapv=-rv*(k2+l2);

dt=0.005;
dto2=dt/2;
dt2=dt*dt;
numsteps=50;
numouts=400;

myfactue=dt*lapu;
myfactve=dt*lapv;

myfactu=(1+dto2*lapu)./(1-dto2*lapu);
myfactv=(1+dto2*lapv)./(1-dto2*lapv);
tsave=[0];
for ii=1:numouts
    save(sprintf('sim/out_%i.mat',ii-1), 'u','v')
 for jj=1:numsteps
   t=t+dt;
   % Strang split two steps at a time reversing order
   
   u=ifft2(myfactu.*fft2(u));
   v=ifft2(myfactv.*fft2(v));

   % RK4
   rkk1=f1(u,v);
   rkl1=f2(u,v);
   rkk2=f1(u+0.5*dt*rkk1,v+0.5*dt*rkl1);
   rkl2=f2(u+0.5*dt*rkk1,v+0.5*dt*rkl1);
   rkk3=f1(u+0.5*dt*rkk2,v+0.5*dt*rkl2);
   rkl3=f2(u+0.5*dt*rkk2,v+0.5*dt*rkl2);
   rkk4=f1(u+dt*rkk3,v+0.5*dt*rkl3);
   rkl4=f2(u+dt*rkk3,v+0.5*dt*rkl3);
   kforrk=(dt/6)*(rkk1+2*rkk2+2*rkk3+rkk4);
   lforrk=(dt/6)*(rkl1+2*rkl2+2*rkl3+rkl4);

   u=u+kforrk;
   v=v+lforrk;
  
%second step in reverse order now
 
  t=t+dt;

   % RK4
   rkk1=f1(u,v);
   rkl1=f2(u,v);
   rkk2=f1(u+0.5*dt*rkk1,v+0.5*dt*rkl1);
   rkl2=f2(u+0.5*dt*rkk1,v+0.5*dt*rkl1);
   rkk3=f1(u+0.5*dt*rkk2,v+0.5*dt*rkl2);
   rkl3=f2(u+0.5*dt*rkk2,v+0.5*dt*rkl2);
   rkk4=f1(u+dt*rkk3,v+0.5*dt*rkl3);
   rkl4=f2(u+dt*rkk3,v+0.5*dt*rkl3);
   kforrk=(dt/6)*(rkk1+2*rkk2+2*rkk3+rkk4);
   lforrk=(dt/6)*(rkl1+2*rkl2+2*rkl3+rkl4);

   u=u+kforrk;%+sigdtrt*randn(size(u));
   v=v+lforrk;%+sigdtrt*randn(size(u));
% diffusion
   u=ifft2(myfactu.*fft2(u));
   v=ifft2(myfactv.*fft2(v));

 end
 
   tsave=[tsave t];

end

% Save u, v, and time
save(sprintf('sim/out_%i.mat',ii), 'u','v')

t=tsave;
save('sim/t.mat','t')

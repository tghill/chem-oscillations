%% Solves reaction-diffusion problem with FKN model reaction.
% Uses ode23s stiff solver for time stepping and finite differences
% for the diffusion.
tic;
% clear all; close all
%% Time stepping parameters
tstart=0;       % start time
dtout=0.2;      % time interval between saved outputs
tend=10;        % end time

steps=(tend-tstart)/dtout;
tt=linspace(tstart,tend,steps+1);

%% Grid parameters
N=32;   % number of points in x and y
L=pi;   % Box size. Domain is [-L, L] x [-L, L]

h=2*L/N;
h2=1./(h^2);

z=linspace(-1,1,N+1);
z=z(1:end-1);

[xx,yy]=meshgrid(L*z,L*z);


%% FKN model parameters (from Murray book)
eps=5e-5;
delta=2e-4;
q=8e-4;
f=0.5;

% Diffusion coefficients (trial and error)
Du=5e-3;
Dv=5e-3;
Dw=1e-4;

%% Initial conditions
r2=xx.*xx + yy.*yy;
expfactor=exp(-r2.^4./(0.25*L)^2);

u=8e-4    + (0.9046-8e-4)*expfactor;
v=24.75   + (0.09535-24.75)*expfactor;
w=3.55e-2 + (0.08672-3.55e-2)*expfactor;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% That should be the end of what you want to change
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Save parameters to structure
para.eps=eps;
para.delta=delta;
para.q=q;
para.f=f;

para.Du=Du;
para.Dv=Dv;
para.Dw=Dw;

para.xx=xx;
para.yy=yy;

[nrows,ncols]=size(u);
n=numel(u);

%% Build the wavenumber grids
dk=pi/L;
ksvec(1)=0; 
for ii=2:(N/2)
   ksvec(ii)=ii-1;
   ksvec(N/2+ii)=-N/2 + ii -1;
end
ksvec=ksvec*dk;
ksvec(N/2+1)=0;
[k,l]=meshgrid(ksvec,ksvec);
k2=k.*k; l2=l.*l;

% Laplacian coefficient
lap=-(k2+l2);

% Initial condition - flatten matrices and concatenate
x0=[u(:); v(:); w(:)];
Nelem=numel(x0);     

% Build RHS function
rhsfun=@(t,y) F_fkn(t,y,eps,delta,q,f) + F_diff_fe(t,y,[Du,Dv,Dw],h2);

%% Time-step
% solver options
options = odeset('RelTol',1e-3,'AbsTol',1e-6);

% This is where we actually solve the equations
[t,y]=ode15s(rhsfun,tt,x0,options);

% Extract solution and reshape to (nt, nx, ny)
u=y(1:end,1:Nelem/3);
v=y(1:end,1+Nelem/3:2*Nelem/3);
w=y(1:end,1+2*Nelem/3:end);

nt=size(y,1);
u=reshape(u,nt,nrows,ncols);
v=reshape(v,nt,nrows,ncols);
w=reshape(w,nt,nrows,ncols);

% Save the output
save('simout.mat', 'u','v','w','t','k','l','para')

toc

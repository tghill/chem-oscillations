# Finite difference FKN model solver
This directory contains matlab code to integrate the FKN model equations in a reaction diffusion framework using finite difference methods.

The script to run the solver is `fkn_solver.m`. The other files are explained below:

 * `F_diff_de.m`: Compute laplacian using finite differences
 * `F_diff.m`: Compute laplacian using spectral methods
 * `F_fkn.m`: Function to compute the RHS of the FKN model equations
 * `post.m`: Tools for post-processing results. Makes a few figures visualizing the model outputs.

This code runs relatively slowly due to the stiff nature of the FKN model equations.

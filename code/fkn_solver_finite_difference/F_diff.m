function F=F_diff(t,U,lap,D)
% Compute diffusion part of reaction-diffsion model. Compared to F_fkn,
% we have to convert u,v,w back into matrix form. Uses spectral
% differentiation for the laplacian.

%% 1: Convert column vector U into matrices u,v,w
% Number of elements in state vector
N=size(U,1);

% Number of rows and columns assuming square matrix
nrows=sqrt(N/3);
ncols=nrows;

% Unpack into matrices vectors for each of the 3 variables
u=reshape(U(1:N/3),nrows,ncols);
v=reshape(U(1+N/3:2*N/3),nrows,ncols);
w=reshape(U(1+2*N/3:end),nrows,ncols);

%% 2: Diffusion
du=ifft2(D(1).*lap.*fft2(u));
dv=ifft2(D(2).*lap.*fft2(v));
dw=ifft2(D(3).*lap.*fft2(w));

%% 3: Build the column vector again
F=[du(:);dv(:);dw(:)];
function F=F_diff_fe(t,U,D,h2)
% Compute diffusion part of reaction-diffsion model. Compared to F_fkn,
% we have to convert u,v,w back into matrix form. Uses spectral
% differentiation for the laplacian.

%% 1: Convert column vector U into matrices u,v,w
% Number of elements in state vector
N=size(U,1);

% Number of rows and columns assuming square matrix
n=sqrt(N/3);
nrows=n;
ncols=n;

% Unpack into matrices vectors for each of the 3 variables
u=reshape(U(1:N/3),nrows,ncols);
v=reshape(U(1+N/3:2*N/3),nrows,ncols);
w=reshape(U(1+2*N/3:end),nrows,ncols);

%% 2: Diffusion
% du=ifft2(D(1).*lap.*fft2(u));
% dv=ifft2(D(2).*lap.*fft2(v));
% dw=ifft2(D(3).*lap.*fft2(w));

up=u( mod(0:n+1,n)+1, mod(0:n+1,n)+1);
vp=v( mod(0:n+1,n)+1, mod(0:n+1,n)+1);
wp=w( mod(0:n+1,n)+1, mod(0:n+1,n)+1);

dux=up(2:end-1,3:end)-2*up(2:end-1,2:end-1)+up(2:end-1,1:end-2);
duy=up(3:end,2:end-1)-2*up(2:end-1,2:end-1)+up(1:end-2,2:end-1);

dvx=vp(2:end-1,3:end)-2*vp(2:end-1,2:end-1)+vp(2:end-1,1:end-2);
dvy=vp(3:end,2:end-1)-2*vp(2:end-1,2:end-1)+vp(1:end-2,2:end-1);

dwx=wp(2:end-1,3:end)-2*wp(2:end-1,2:end-1)+wp(2:end-1,1:end-2);
dwy=wp(3:end,2:end-1)-2*wp(2:end-1,2:end-1)+wp(1:end-2,2:end-1);

du=-D(1).*h2.*(dux+duy);
dv=-D(2).*h2.*(dvx+dvy);
dw=-D(3).*h2.*(dwx+dwy);

%% 3: Build the column vector again
F=[du(:);dv(:);dw(:)];
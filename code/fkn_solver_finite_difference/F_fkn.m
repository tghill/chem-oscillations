function F=F_fkn(t,U,eps,delta,q,f)
% RHS of the FKN system. U is a column vector containing
%  flattened matrices for each of the 3 state variables.

% Number of elements in state vector
N=size(U,1);

% Unpack into column vectors for each of the 3 variables
u=U(1:N/3);
v=U(1+N/3:2*N/3);
w=U(1+2*N/3:end);

% Compute time derivatives for each component
fkn1=(1/eps)*(q.*v - u.*v + u.*(1-u));
fkn2=(1/delta)*(-q.*v - u.*v + 2.*f.*w);
fkn3=u-w;

% Build the column vector again
F=[fkn1;fkn2;fkn3];
%% A doubly periodic reaction diffusion model for FKN model equations.
% Simulation outputs are saved to the fkn_simout directory.
N=256;

%% Grid parameters
z=linspace(-1,1,N+1);
z=z(1:end-1);
L=pi;
[x,y]=meshgrid(L*z,L*z);

t=0;

%% FKN parameters
eps=5e-5;
delta=2e-4;
q=8e-4;
f=0.5;

% Diffusion constants
ru=1e-2; rv=1e-2; rw=1e-2;

%% FKN equations
fkn1=@(u,v,w) (1/eps)*(q.*v - u.*v + u.*(1-u));
fkn2=@(u,v,w) (1/delta)*(-q.*v - u.*v + 2*f*w);
fkn3=@(u,v,w) (u-w);

%% FKN initial conditions
r2=x.*x+y.*y;
expfactor1=exp(-(((x+0.5*L).^2 + y.^2).^8)/((0.25*L)^8));
expfactor2=exp(-(((x-0.5*L).^2 + y.^2).^8)/((0.25*L)^8));
expfactor3=exp(-(((x).^2 + (y+0.5*L).^2).^8)/((0.25*L)^8));
expfactor4=exp(-(((x).^2 + (y-0.5*L).^2).^8)/((0.25*L)^8));

expfactor = expfactor1+expfactor2 + expfactor3 + expfactor4;
u0=8e-4    + (0.9046-8e-4)*expfactor;
v0=24.75   + (0.09535-24.75)*expfactor;
w0=3.55e-2 + (0.08672-3.55e-2)*expfactor;

u=u0; v=v0; w=w0;

% Compute wavevectors
dk=pi/L;
ksvec(1)=0;
for ii=2:(N/2)
   ksvec(ii)=ii-1;
   ksvec(N/2+ii)=-N/2 + ii -1;
end
ksvec=ksvec*dk;
% [k2,l2]=meshgrid(ksvec.^2,ksvec.^2);
ksvec(N/2+1)=0;
[k,l]=meshgrid(ksvec,ksvec);
k2=k.*k; l2=l.*l;
% Laplacians are easy in wavenumber space
lapu=-ru*(k2+l2);
lapv=-rv*(k2+l2);
lapw=-rw*(k2+l2);

%% Time stepping parameters
dt=2e-7;
dto2=dt/2;
dt2=dt*dt;
numsteps=5e4;%200;
numouts=200;%80

% Save initial conditions
save('fkn_simout/simout_000.mat','u','v','w')

% Now solve the equations
myfactue=dt*lapu;
myfactve=dt*lapv;
myfactwe=dt*lapw;

myfactu=(1+dto2*lapu)./(1-dto2*lapu);
myfactv=(1+dto2*lapv)./(1-dto2*lapv);
myfactw=(1+dto2*lapw)./(1-dto2*lapw);

tout=[0];

%% RK4 built explicitly for the vector system
for ii=1:numouts
  t
  for jj=1:numsteps
    t=t+dt;

    % Strang split two steps at a time reversing order

    u=ifft2(myfactu.*fft2(u));
    v=ifft2(myfactv.*fft2(v));
    w=ifft2(myfactw.*fft2(w));

    % RK4
    rkk1=fkn1(u,v,w);
    rkl1=fkn2(u,v,w);
    rkm1=fkn3(u,v,w);

    rkk2=fkn1(u+0.5*dt*rkk1,v+0.5*dt*rkl1,w+0.5*dt*rkm1);
    rkl2=fkn2(u+0.5*dt*rkk1,v+0.5*dt*rkl1,w+0.5*dt*rkm1);
    rkm2=fkn3(u+0.5*dt*rkk1,v+0.5*dt*rkl1,w+0.5*dt*rkm1);

    rkk3=fkn1(u+0.5*dt*rkk2,v+0.5*dt*rkl2,w+0.5*dt*rkm2);
    rkl3=fkn2(u+0.5*dt*rkk2,v+0.5*dt*rkl2,w+0.5*dt*rkm2);
    rkm3=fkn3(u+0.5*dt*rkk2,v+0.5*dt*rkl2,w+0.5*dt*rkm2);

    rkk4=fkn1(u+dt*rkk3,v+0.5*dt*rkl3,w+0.5*dt*rkm3);
    rkl4=fkn2(u+dt*rkk3,v+0.5*dt*rkl3,w+0.5*dt*rkm3);
    rkm4=fkn3(u+dt*rkk3,v+0.5*dt*rkl3,w+0.5*dt*rkm3);

    kforrk=(dt/6)*(rkk1+2*rkk2+2*rkk3+rkk4);
    lforrk=(dt/6)*(rkl1+2*rkl2+2*rkl3+rkl4);
    mforrk=(dt/6)*(rkm1+2*rkm2+2*rkm3+rkm4);

    u=u+kforrk;
    v=v+lforrk;
    w=w+mforrk;

    %second step in reverse order now

    t=t+dt;

    % RK4
    rkk1=fkn1(u,v,w);
    rkl1=fkn2(u,v,w);
    rkm1=fkn3(u,v,w);

    rkk2=fkn1(u+0.5*dt*rkk1,v+0.5*dt*rkl1,w+0.5*dt*rkm1);
    rkl2=fkn2(u+0.5*dt*rkk1,v+0.5*dt*rkl1,w+0.5*dt*rkm1);
    rkm2=fkn3(u+0.5*dt*rkk1,v+0.5*dt*rkl1,w+0.5*dt*rkm1);

    rkk3=fkn1(u+0.5*dt*rkk2,v+0.5*dt*rkl2,w+0.5*dt*rkm2);
    rkl3=fkn2(u+0.5*dt*rkk2,v+0.5*dt*rkl2,w+0.5*dt*rkm2);
    rkm3=fkn3(u+0.5*dt*rkk2,v+0.5*dt*rkl2,w+0.5*dt*rkm2);

    rkk4=fkn1(u+dt*rkk3,v+0.5*dt*rkl3,w+0.5*dt*rkm3);
    rkl4=fkn2(u+dt*rkk3,v+0.5*dt*rkl3,w+0.5*dt*rkm3);
    rkm4=fkn3(u+dt*rkk3,v+0.5*dt*rkl3,w+0.5*dt*rkm3);

    kforrk=(dt/6)*(rkk1+2*rkk2+2*rkk3+rkk4);
    lforrk=(dt/6)*(rkl1+2*rkl2+2*rkl3+rkl4);
    mforrk=(dt/6)*(rkm1+2*rkm2+2*rkm3+rkm4);

    u=u+kforrk;
    v=v+lforrk;
    w=w+mforrk;

    % diffusion
    u=ifft2(myfactu.*fft2(u));
    v=ifft2(myfactv.*fft2(v));
    w=ifft2(myfactw.*fft2(w));
  end

  tout=[tout t];
  save(sprintf('fkn_simout/simout_%03d.mat',ii),'u','v','w')
end
% Save parameters
save('fkn_simout/params.mat','k','l','ru','rv','rw','delta','eps','q','f','tout')

# Oscillating chemical reactions
Repository for code and docs for chemical oscillations group project for AMATH 732.

Online documentation is hosted on [readthedocs](http://chemical-oscillations.rtfd.io/).

The code is all in the `code/` directory, where there is a README file explaining each of the different cases. In summary, the code in this repository solves the Brusselator and FKN (Field-Koros-Noyes, or Oregonator) model equations in ODE and two-dimensional reaction diffusion frameworks.

# Installation
All you need to do to get started running and modifying this code is to clone the repository. We recommend starting out with the Brusselator model, and then moving up to the FKN model.
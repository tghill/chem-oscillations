# ODE Results
Recall the simplified model, the Brusselator:

<img src= "https://git.uwaterloo.ca/tghill/chem-oscillations/raw/tim/docs/data/brusselator.png" width="350" height="225"/>

This is the simplest realistic model of an auto-catalytic, oscillatory chemical reaction. The reaction dynamics do not fully capture the behaviour of the Belousov–Zhabotinsky reaction, but instead provides a simple toy model for studying the general behaviour of oscillatory systems, and later in developing reaction diffusion frameworks.

First, we solve the system of ODEs. Analyzing the system, it's easy to show that there is a nontrivial equilibrium point at (A, B/A) which is stable when B &le; 1 + A<sup>2</sup>. For B &gt; 1 + A<sup>2</sup> the equilibrium point is unstable, and there is a stable limit cycle. In other words, there is a supercritical Hopf bifurcation when B=1+A<sup>2</sup>.


The system is simulated for parameteter combinations listed below:

 A  | B   | Case name
--- | --- | ---
 1  |  1  |  steady state
 1  | 1.9 |  subcritical
 1  | 2.0 |  critical
 1  | 2.1 |  supercritical

The trajectories and phase plots are shown below. Solutions are directly from the `code/brusselator_ODE` solver. This code runs very quickly, and so is easy to run a number of parameter sets to explore the system's behaviour.

Steady state:

<img src= "https://git.uwaterloo.ca/tghill/chem-oscillations/raw/tim/docs/data/Brusselator_1traj_ss.png" width="350" height="225"/>
<img src= "https://git.uwaterloo.ca/tghill/chem-oscillations/raw/tim/docs/data/Brusselator_1phase_ss.png" width="350" height="225"/>

Subcritical:

<img src= "https://git.uwaterloo.ca/tghill/chem-oscillations/raw/tim/docs/data/Brusselator_1traj_subcrit.png" width="350" height="225"/>
<img src= "https://git.uwaterloo.ca/tghill/chem-oscillations/raw/tim/docs/data/Brusselator_1phase_subcrit.png" width="350" height="225"/>

Critical:

<img src= "https://git.uwaterloo.ca/tghill/chem-oscillations/raw/tim/docs/data/Brusselator_1traj_crit.png" width="350" height="225"/>
<img src= "https://git.uwaterloo.ca/tghill/chem-oscillations/raw/tim/docs/data/Brusselator_1phase_crit.png" width="350" height="225"/>

Supercritical:

<img src= "https://git.uwaterloo.ca/tghill/chem-oscillations/raw/tim/docs/data/Brusselator_1traj_supcrit.png" width="350" height="225"/>
<img src= "https://git.uwaterloo.ca/tghill/chem-oscillations/raw/tim/docs/data/Brusselator_1phase_supcrit.png" width="350" height="225"/>

The above trajectories and phase plots show the expected behaviour near B=1+A<sup>2</sup>. The limit cycle evident in the supercritical case is what we want to simulate in a 2 dimensional reaction diffusion model.

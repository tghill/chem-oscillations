# Reaction Diffusion Results

## Reaction diffusion system
Informed by the ODE results, the Brusselator system is implemented as a reaction diffusion system. Using the framework from the [background section](https://chemical-oscillations.readthedocs.io/en/latest/BZchem/derivation/), the reaction diffusion system is

<img src= "https://git.uwaterloo.ca/tghill/chem-oscillations/raw/master/docs/data/brusselator_rd.png" width="350" height="225"/>

where D<sub>x</sub> and D<sub>y</sub> are diffusion constants.

## Numerical method and initial conditions
The system is solved using spectral differentiation for the diffusion, and RK4 time stepping with time step dt=0.001 for t=[0,50]. The equations are implemented on a periodic square domain, [-2.5, 2.5] x [-2.5, 2.5], with N=256 points in x and y. The code can be found in the repository in `chem-oscillations/code/brusselator_reaction_diffusion/brusselator_solver.m`.

The initial conditions are informed by the ODE results. From the limit cycle, we find two points out of phase from each other:

<img src= "https://git.uwaterloo.ca/tghill/chem-oscillations/raw/master/docs/data/Brusselator_IC.png" width="500" height="300"/>

A super-gaussian hump (proportional to r<sup>8</sup>) is set in the center of the domain using the values at the second red line above, and the rest of the domain is intialized at the values at the first red line above. Gaussian noise with standard deviation 0.1 and mean 0 is superimposed on top.

## Results
The code for these simulations is in the `code/brusselator_reaction_diffusion` directory.

An animation showing the concentrations X and Y is shown in the embedded video below.
<iframe src="https://player.vimeo.com/video/376876525" width="640" height="564" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

Notice the center and edges remain out of phase, with the bulge slowly diffusion outward. Near the end there is evidence of spectral ringing, indicating we should be using a finer grid.

## Changing the initial conditions
Inspired by the spectral ringing in the previous example, we increase the number of grid points to 512 in x and y. The initial conditions are changed to have four super-gaussians. This case shows more evidence of steady pattern formation and less spectral ringing.

<iframe src="https://player.vimeo.com/video/376876497" width="640" height="564" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

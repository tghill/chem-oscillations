# Welcome
Welcome to the online documentation for the Oscillating Chemical Reactions project for AMATH 732, Fall 2019.

This website investigates a few mathematical models for chemical reactions that oscillate in time. [This video](https://www.youtube.com/watch?v=LL3kVtc-4vY&t=915s) shows a demonstration of one such reaction, the Belousov-Zhabotinsky reaction (see the model background section for more information).

The [repository](https://git.uwaterloo.ca/tghill/chem-oscillations) has code to solve the resultant equations in various methods and configurations.

The purpose of this website is to describe the background theory and model formulation, and describe the results obtained from the code in the git repository. The information here should allow you to design your own experiments and adapt the code as you wish.

## Organization
This documentation is organized into the following sections:

* **Model Background** describes the chemistry background, and how you construct the mathematical model from the chemical equations.
* **Brusselator Model** describes the implementation and results of the Brusselator ODE and reaction-diffusion models.
* **FKN Model** describes the implementation results of the Field-Koros-Noyes (FKN, or Oregonator) model as an ODE system, and preliminary attempts at a reaction-diffusion solution.

The code lives in the [repository](https://git.uwaterloo.ca/tghill/chem-oscillations) in the `code/` directory. Feel free to check out the code there and run and modify the cases for yourself. See more details on running the code on the Getting Started page.

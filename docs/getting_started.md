# Getting started
To experiment with the code yourself, all you need to do is clone the git repository:

```
git clone https://git.uwaterloo.ca/tghill/chem-oscillations.git
```

## Description of the solvers
The git repo has four main solvers you can run and investigate:

* `brusselator_ODE`: Integrate the Brusselator ODE system. This code runs very fast and is good for parameter explorations and informing choices you make in running the reaction diffusion code.
* `brusselator_reaction_diffusion`: Integrates the Brusselator reaction diffusion system. Diffusion is computed spectrally, and RK4 time stepping is explicitly implemented. This code is good for exploring different initial conditions and pattern formation.
* `fkn_solver_finite_difference`: Solves the FKN reaction diffusion system using finite differences for spatial derivatives and built in MATLAB stiff ODE solvers for the time stepping.
* `fkn_solver_spectral`: The equivalent of the `brusselator_reaction_diffusion` code for the FKN system. Uses spectral differentiation and RK4 time stepping. This code is relatively unstable still - feel free to improve it!

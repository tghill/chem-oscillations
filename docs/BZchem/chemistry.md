<h2>Brief Chemistry Background</h2>

A simple chemical reaction of two reactants and two products can be expressed as:

<img src= "https://git.uwaterloo.ca/tghill/chem-oscillations/raw/master/docs/data/alpha_A_+_beta_B.png" width="200" height="20"/>

where the Greek letters correspond to stoichiometric coefficients, which balance the chemical reaction, and the upper-case letters represent the chemical species involved.
A very special case is that of chemical equilibrium, in which the forward and reverse reactions rates are such that each chemical species involved in the reaction
is being created at a rate proportional to that at which it is being destroyed.<sup>[1](#myfootnote1)</sup> Mathematically, this can be stated as follows. 

<img src= "https://git.uwaterloo.ca/tghill/chem-oscillations/raw/master/docs/data/eq2.png" width="250" height="25"/>

where k<sub>&pm;</sub> are the associated rate constants that serve to quantify the rate of the chemical reaction. In instances that stray far from equilibrium, the forward and 
reverse reaction rates no longer balance. That is, the concentration of reactants and products changes in time. For example, let's consider the reactant chemical 
species, A, in the above equation. Every forward reaction consumes &alpha; molecules of A, while every reverse reaction creates &alpha; molecules of A. The 
rate of change in the number of moles of A can expressed by the following differential equation

<img src= "https://git.uwaterloo.ca/tghill/chem-oscillations/raw/master/docs/data/eq3.png" width="325" height="50"/>

Note that the brackets above indicate the concentration of the chemical species in moles per litre. Similar equations can be derived for the remaining chemical reactant,
B, and the two products, S and T, using the law of mass action.<sup>[1](#myfootnote1)</sup> In chemistry, the law of mass action is the assumption that the rate of a chemical reaction is proportional to the concentrations of the reactants, or more 
specifically, the product of the activities of the reactants.<sup>[2](#myfootnote2)</sup> Strickly speaking, this hypothesis is only true for elementary reactions. An elementary reaction is 
a reaction in which one or more chemical species react directly to form products in a single mechanistic step - no reaction intermediates are produced. In general,
the majority of interesting chemical reactions occur with the formation of reactive intermediates. However, all reactions can typically be represented 
as a series of elementary reactions, such that the overall rate equation can be derived from the individual steps. Thus, the law of mass action is considered
universal and can be applied in these complicated cases.

<h3>Autocatalytic Reactions</h3>

An interesting type of reaction is an autocatalytic reaction, which has the key property that at least one of the products is a reactant. Perhaps the most straightforward
example is given by:

<img src= "https://git.uwaterloo.ca/tghill/chem-oscillations/raw/master/docs/data/eq4.png" width="125" height="20"/>

with corresponding rate equations (again derived using the law of mass action)

<img src= "https://git.uwaterloo.ca/tghill/chem-oscillations/raw/master/docs/data/eq5.png" width="250" height="100"/>

The key feature here is that this set of equations is non-linear, a fact which can lead to multiple fixed points of the system, ultimately allowing multiple states of the 
system to exist. Autocatalytic reactions proceed slowly at the start because there is little catalyst present.<sup>[1](#myfootnote1)</sup> Recall that a catalyst is a substance that 
increases the rate of a chemical reaction without itself undergoing any permanent chemical change. The rate of reaction increases as the amount of catalyst 
increases and the reaction proceeds onward, slowing down only as the reactant concentration begins to decrease. 

One example of an autocatalytic reaction is a clock reaction. Commonly referred to as a chemical clock, such a reaction involves a mixture of chemical compounds
in which an observerable property occurs after a calculable time lapse. In other words, the concentration of the reaction intermediates oscillate in time. If one
of the chemical reagents has a distinct, visible colour, this effect can be seen as an abrupt colour change. A real world example of a clock reaction is the 
Belousov-Zhabotinsky reaction, an osicllatory reaction in which the concentrations of the reaction constituents can be approximated in terms of damped osciallations.<sup>[1](#myfootnote1)</sup>


<h2>Belousov-Zhabotinsky (BZ) Oscillating Reactions</h2>

The Belousov-Zhabotinksy, or BZ, reaction for short, is likely the most widely studied oscillating reaction both theoretically and experimentally. One of the main reasons
for this is due to the fact that this model can be applied to a plethora of biological systems.<sup>[4](#myfootnote4)</sup> In regards to the BZ reaction as it applies to chemical systems,
oscillations in colour are observed as the system cycles through its component reaction pathways. That is to say, the system will oscillate in colour until equilibrium
is achieved, where the frequency of these oscillations is a function of the initial concentration of the chemical constituents. The reaction itself involves the oxidation
of an organic substrate, typically by bromate ions, BrO<sub>3</sub><sup>-</sup>, in the presence of an acid. The reaction is catalyzed by cerium which has two states,
namely, Ce<sup>3+</sup> (cerous ions), and Ce<sup>4+</sup> (ceric ions). The concentration of the catalyst ions vary in time, giving rise to sustained periodic
oscillations which can be readily observed as illustrated in the figure below. <sup>[3](#myfootnote3)</sup>


![alt text](https://git.uwaterloo.ca/tghill/chem-oscillations/raw/master/docs/data/BZ_oscillations.jpg)

Essentially, the reaction can be broken into two parts with an autocatalytic step drawing the divide. It is the concentration of bromine that 
ultimately determines which step is dominant at any given time. Suppose the system initially has a high concentration of bromide ions, [Br<sup>-</sup>].
In the first stage of the reaction, the concentration of bromide ions is high and as a result these are the reactants to be consumed. At this stage, the cerium present
is in the cerous state, or equivalently, it is the Ce<sup>3+</sup> ions that are dominant. The reaction medium remains colourless for this phase of the cycle. As 
the reaction proceeds, the concentration of the bromide ions decreases further, ultimately reaching a critical value at which point the concentration drops quickly. 
Here as we enter the second stage of the reaction, the cerium changes from the cerous to the ceric state and the reaction medium turns yellow. It is this reaction 
pathway, in which the Ce<sup>4+</sup> ions first present themselves, that contains the autocatalytic step. As the cerous ions are consumed and ceric ions accumulate, 
a critical threshold is achieved at which time a third pathway opens. That is, the ceric ions react to produce bromide ions, Br<sup>-</sup>, ultimately regenerating 
Ce<sup>3+</sup>. The system returns to a ceric state and the reaction medium turns clear once more. As the bromide ion concentration increases, processes involved
with stage one will then repeat. Thus the system has completed its closed cycle.<sup>[3](#myfootnote3),</sup> <sup>[4](#myfootnote4)</sup>

Although the BZ reaction involves many chemical pathways, it can be reduced to a series of five reactions with known values for the associated rate constants. The
most common dynamical model for the BZ reaction, the Oregonator, is derived from this sequence of chemical reactions:<sup>[3](#myfootnote3)</sup>

<img src= "https://git.uwaterloo.ca/tghill/chem-oscillations/raw/master/docs/data/eq6.png" width="200" height="150"/> 

where the variables above represent the following elements

<img src= "https://git.uwaterloo.ca/tghill/chem-oscillations/raw/master/docs/data/eq8.png" width="900" height="30"/>

The associated chemical names are bromous acid (X), bromate (A), bromine with a negative ionic charge (Y), hypobromous acid (P), malonic acid (B), and the ceric ion of cesium (Z). Note that the first two reactions 
above are roughly equivalent to the first stage as previously described, while the last three relate approximately to process two.It is the third chemical reaction 
that is autocatalytic. 

The reaction can be ran in either a continuously stirred tank reactor, or an undistrubed petri dish. In the former case, the rates of input of the reactants
and output of the products can be varied throughout the experiment. That is, the distance of the BZ system from a state of chemical equilibrium can be precisely
controlled and temporal oscillations in colour are readily observed. On the other hand, if the experiment is carried out with a thin film of reactant in a petri dish,
the reactants are able to diffuse, exhibiting complex spatiotemporal patterns. Typically, concentric rings patterns are observed which propgate throughout the
reduced medium, away from the point at which they were initiated. The outward propragation can be explained by the diffusion of bromous acid ahead of the wave front
and its autocatalytic production behind. Colour changes are observed as the waves of oxidation spread outwards.<sup>[4](#myfootnote4)</sup>

<a name="myfootnote1">1</a>: Wikipedia: The Free Encycolopedia. (2019). Autocatalysis. Retrieved from https://en.wikipedia.org/wiki/Autocatalysis

<a name="myfootnote2">2</a>: Wikipedia: The Free Encycolopedia. (2019). Law of mass action. Retrieved from https://en.wikipedia.org/wiki/Law_of_mass_action

<a name="myfootnote3">3</a>: Murray, J.D. (2002). Mathematical Biology I: An Introduction. New York, NY: Springer Science+Business Media, Inc.

<a name="myfootnote4">4</a>:  Shanks, Niall (2001). Modeling biological systems: The belousov–zhabotinsky reaction. _Foundations of Chemistry_ 3 (1):33-53.
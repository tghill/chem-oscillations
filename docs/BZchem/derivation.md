<h2>Mathematical Derivation</h2>

<h3>The Oregonator (Field-Koros-Noyes Model)</h3>

The Oregonator is considered to be the central dynamical model for the BZ reaction, providing a simple yet realistic description of the chemical dynamics involved.<sup>[1](#myfootnote1)</sup> 
The model equations are derived from the previously preseneted reduced set of five chemical reactions (see [chemistry](https://chemical-oscillations.readthedocs.io/en/latest/BZchem/chemistry/) section of documentation). The concentrations
of the chemical species represed by the letters A, B and P are held constant throughtout the reaction since they enter and exit the reduced medium at constant 
rates. Recall, these chemicals correspond to the bromate ions, bromomalonic acid, and hypobromous acid, respectively. By contrast, the concentrations of the 
chemical constiuents X, Y, and Z change in time. That is, the concentrations of the bromous acid, the bromide ions, and the ceric ions of cesium - all of which are
involved in the autocatalytic step of the reaction - vary with time. Given this, the FKN model can be expressed in terms of three coupled ordinary differential 
equations. Using the law of mass action, the following equations have been derived to describe the complex dynamics of the system.<sup>[1,](#myfootnote1)</sup><sup>[2](#myfootnote1)</sup>

<img src= "https://git.uwaterloo.ca/tghill/chem-oscillations/raw/master/docs/data/eq9.png" width="300" height="150"/>

Here the rate constants, k<sub>1</sub> through k<sub>5</sub>, are experimentally determined and f is an adjustable stoichiometric factor, usually taken to be 0.5. The 
Oregonator model of the BZ reaction involves extensive idealization and simplification of the associated chemistry for the system. The actual reaction involves
more than twenty different chemical species, such that a model reflecting the actual dynamics would involve more than twenty differential equations and the determination
of an equal number of rate constants.<sup>[1](#myfootnote1)</sup> The model however saves the interesting dynamical behaviour such that many conclusions can be made, simplifications aside.

The model may be nondimensionalized, and written in vector form as follows:<sup>[2](#myfootnote2)</sup>

<img src= "https://git.uwaterloo.ca/tghill/chem-oscillations/raw/master/docs/data/math_eq3.png" width="300" height="100"/>

where

<img src= "https://git.uwaterloo.ca/tghill/chem-oscillations/raw/master/docs/data/math_eq4.png" width="350" height="225"/>

When the reaction is run numerically, the rates of input of reactants and outputs of products can be varied, such that we can simulate the behaviour of the system
as it enters a variety of dynamical regimes. As previously discussed, at some distances from chemical equilibrium the system behaves as a chemical clock, with
oscillating colour changes. Other regimes may produce solutions in which periodic behavioiur is not possible, highlighting qualitatively new behaviours of the system.
There exists the freedom to manipulate the above parameter values, but a useful set is given as follows:<sup>[3](#myfootnote3)</sup>

<img src= "https://git.uwaterloo.ca/tghill/chem-oscillations/raw/master/docs/data/math_eq5.png" width="125" height="125"/>


The above system is consistent with observing the reaction in a continuously stirred tank reactor. If we are interested in modelling the experiment as though there
was a thin film of reactant in a undisturbed petri dish, we must reformulate the equations into a reaction-diffusion problem. This new set of equations captures the
effects of diffusion, as the coloured wave fronts propagate outward in time.

<img src= "https://git.uwaterloo.ca/tghill/chem-oscillations/raw/master/docs/data/math_eq6.png" width="200" height="50"/>

Here D is a diagonal matrix of diffusion coefficients, R(q), the reaction term, is given by the right hand side of the above ODE system, and

<img src= "https://git.uwaterloo.ca/tghill/chem-oscillations/raw/master/docs/data/math_eq7.png" width="100" height="90"/>

An analysis of each system presented will be discussed in greater detail later on in the documentation.

<h3>Brusselator: A Simplified Version</h3>

In the case of oscillations observed when the reaction is run in a tank reactor, a simple two-variable model, known as the Brusselator can be employed. This model
essentially ignores the fifth chemical reaction of the BZ model, but retains an autocatalytic step. The chemical species are not necessarily the same as those listed previously.

<img src= "https://git.uwaterloo.ca/tghill/chem-oscillations/raw/master/docs/data/eq11.png" width="150" height="125"/>

Under conditions where the concentrations of A and B are in excess and can be modelled as constants, the rate equations become:

<img src= "https://git.uwaterloo.ca/tghill/chem-oscillations/raw/master/docs/data/eq13.png" width="275" height="125"/>

where the rate constants have been set equal to one for simplicity's sake. This model is one of the most elementary to study autocatalyzing chemical reactions.<sup>[4](#myfootnote4)</sup>

<a name="myfootnote1">1</a>:  Shanks, Niall (2001). Modeling biological systems: The belousov–zhabotinsky reaction. _Foundations of Chemistry_ 3 (1):33-53.

<a name="myfootnote2">2</a>: Scholarpedia. (2011). Oregonator. Retrieved from http://www.scholarpedia.org/article/Oregonator

<a name="myfootnote3">3</a>: Murray, J.D. (2002). Mathematical Biology I: An Introduction. New York, NY: Springer Science+Business Media, Inc.

<a name="myfootnote4">4</a>: Wikipedia: The Free Encycolopedia. (2019). Brusselator. Retrieved from https://en.wikipedia.org/wiki/Brusselator
